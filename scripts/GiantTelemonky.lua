local action = require "necro.game.system.Action"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local ecs = require "system.game.Entities"
local enum = require "system.utils.Enum"
local event = require "necro.event.Event"
local field = components.field
local levelTransition = require "necro.client.LevelTransition"
local objectEvents = require "necro.game.object.ObjectEvents"
local player= require "necro.game.character.Player"
local rng = require "necro.game.system.RNG"
local settings = require "necro.config.Settings"
local sizeModifier = require "necro.game.character.SizeModifier"
local spell = require "necro.game.spell.Spell"

local channel = 6352345987

local whichMonkeys = enum.sequence {
    ALL_MONKEYS = 1,
    ALL_PURPLE_MONKEYS = 2,
    GIANT_PURPLE_MONKEYS = 3
}

WhichMonkeys = settings.shared.enum {
    name = "Monkeys That Teleport",
    enum = whichMonkeys,
    default = whichMonkeys.GIANT_PURPLE_MONKEYS,
}

GiantChance = settings.shared.percent {
    name = "Percent Purple Gigantic",
    default = 0.15,
    maximum = 1,
    minimum = 0,
    step = 0.05
}

components.register{
    trueTeleport = {},
    teleportOnLevelGen = {field.bool("active", false)},
}

commonSpell.registerSpell("SpellcastRecall", {
    spellcast = {},
    spellcastProcessImmediately = {},
    soundSpellcast = {sound = "teleport"},
    GiantTelemonkey_trueTeleport = {}
})

event.entitySchemaLoadPlayer.override("addCommonCharacterComponents", {sequence = 1}, function(func, ev)
    ev.entity.GiantTelemonkey_teleportOnLevelGen = {active = false}
    func(ev)
end)

event.entitySchemaLoadNamedEnemy.override("loadMonkey", {sequence = 1}, function(func, ev)
    if ev.data.type == 3 then --if it's a purple monkey, make it able to be gigantic
        ev.entity.gigantism = {}
    end
    func(ev)
end)

event.spellcast.add("spellcastTrueTeleport", {order = "teleport", sequence = 1, filter = "GiantTelemonkey_trueTeleport"}, function(_)
    levelTransition.switchToLevel(rng.range(1,20, channel))
end)

event.objectSpawn.add("activatePurpleGigantism", {order = "sizeModifier", filter = {"gigantism", "grabTeleportVictim"}, sequence = -1}, function(ev)
    if rng.roll(GiantChance, channel) then
        objectEvents.fire("grow", ev.entity)
        objectEvents.fire("updateScale", ev.entity, {scale = 1})
    end
end)

local function teleportEverybody(entity)
    spell.cast(entity, "GiantTelemonkey_SpellcastRecall")
    for _,p in ipairs(player.getPlayerEntities()) do
        p.GiantTelemonkey_teleportOnLevelGen.active = true
    end
end

event.objectDirection.override("attackGrabber", {sequence = 1, filter = "grabbable"}, function(func, ev)
    local grabber = ecs.getEntityByID(ev.entity.grabbable.grabber)
    if grabber and not grabber.gameObject.despawned and ev.result ~= action.Result.INHIBIT then
        if WhichMonkeys == whichMonkeys.ALL_MONKEYS then
            teleportEverybody(ev.entity)
        elseif WhichMonkeys == whichMonkeys.ALL_PURPLE_MONKEYS and grabber:hasComponent("grabTeleportVictim") then
            teleportEverybody(ev.entity)
        elseif grabber:hasComponent("grabTeleportVictim") and sizeModifier.isGigantic(grabber) then
            teleportEverybody(ev.entity)
        else
            func(ev)
        end
    end
end)

event.gameStateLevel.add("teleportGiantTelemonkeyVictims", {sequence = 1, order = "damageCountdown"}, function(_)
    for _, p in ipairs(player.getPlayerEntities()) do
        if p.GiantTelemonkey_teleportOnLevelGen.active then
            spell.cast(p, "SpellcastTeleportRandom")
            p.GiantTelemonkey_teleportOnLevelGen.active = false
        end
    end
end)