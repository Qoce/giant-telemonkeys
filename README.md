This mod adds gigantic telemonkies which, istead of just teleporting one player, teleports the player who took it and everyone else to a random level, and then teleports everyone to random spots in that level.

Has a setting to select which monkies have this teleport affect. The default is "Giant Purple Monkeys", which is the intended use, but you can also have it such that all purple monknies do this, or such that all monkeys of any color do this.

Has another adjustable setting to select which percentage of purple monkies are turned into gigantic purple monkeys. Default is 15%.